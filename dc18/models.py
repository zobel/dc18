from django.conf import settings
from django.db import models


class AttendeeList(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='attendee_list',
                                on_delete=models.PROTECT)
    name = models.CharField(max_length=50)
    country = models.CharField(max_length=2)
