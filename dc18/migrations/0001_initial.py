# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-08-10 18:08
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AttendeeList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('country', models.CharField(max_length=2)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, related_name='attendee_list', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
