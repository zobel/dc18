---
name: Dinner
---
Usually, dinner is eaten at the end of the day.

Served from 18:30–20:00 daily, in Restaurant 2.
