---
name: Breakfast
---
The most important meal of the day!

Served from 8:30–10am daily, in Restaurant 2.
