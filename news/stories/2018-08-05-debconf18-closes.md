---
title: DebConf18 closes in Hsinchu and DebConf19 dates announced
---

<div class='row'>
  <p>
    <a href='https://wiki.debconf.org/wiki/DebConf18/GroupPhoto'>
      <img class='col-lg-12' src='/static/img/Debconf18_group_photo_small.jpg' title='DebConf18 group photo - click to enlarge' alt='DebConf18 group photo'>
    </a>
  </p>
</div>

Today, Sunday 5 August 2018, the annual Debian Developers
and Contributors Conference came to a close.
With over 306 people attending from all over the world,
and 137 events including 100 talks, 25 discussion sessions or BoFs,
5 workshops and 7 other activities,
[DebConf18](https://debconf18.debconf.org) has been hailed as a success.

Highlights included DebCamp with more than 90 participants,
the [Open Day](https://debconf18.debconf.org/schedule/?day=2018-07-28),
where events of interest to a broader audience were offered,
plenaries like the traditional Bits from the DPL,
a Questions and Answers session with Minister Audrey Tang,
a panel discussion about "Ignoring negativity" with Bdale Garbee, Chris Lamb, Enrico Zini and Steve McIntyre,
the talk "That's a free software issue!!" given by Molly de Blanc and Karen Sandler,
lightning talks and live demos
and the announcement of next year's DebConf ([DebConf19](https://wiki.debian.org/DebConf/19) in Curitiba, Brazil).

The [schedule](https://debconf18.debconf.org/schedule/)
has been updated every day, including 27 ad-hoc new activities, planned
by attendees during the whole conference.

For those not able to attend, most talks and sessions were recorded and live streamed,
and videos are being made available at the
[Debian meetings archive website](https://meetings-archive.debian.net/pub/debian-meetings/2018/DebConf18/).
Many sessions also facilitated remote participation via IRC or a collaborative text document.

The [DebConf18](https://debconf18.debconf.org/) website
will remain active for archive purposes, and will continue to offer
links to the presentations and videos of talks and events.

Next year, [DebConf19](https://wiki.debian.org/DebConf/19) will be held in Curitiba, Brazil,
from 21 July to 28 July, 2019. It will be the second DebConf held in Brazil
(first one was DebConf4 in Porto Alegre).
For the days before DebConf the local organisers will again set up DebCamp
(13 July – 19 July),
a session for some intense work on improving the distribution,
and organise the Open Day on 20 July 2019, open to the general public.


DebConf is committed to a safe and welcome environment for all participants.
See the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml)
and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct) for more details on this.


Debian thanks the commitment of numerous [sponsors](https://debconf18.debconf.org/sponsors/) 
to support DebConf18, particularly our Platinum Sponsor
[Hewlett Packard Enterprise](http://www.hpe.com/engage/opensource).


### About Debian

The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
_universal operating system_.

### About DebConf

DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
[https://debconf.org/](https://debconf.org).

### About Hewlett Packard Enterprise

[Hewlett Packard Enterprise (HPE)](http://www.hpe.com/engage/opensource)
is an industry-leading technology company
providing a comprehensive portfolio of products such as
integrated systems, servers, storage, networking and software.
The company offers consulting, operational support, financial services,
and complete solutions for many different industries: mobile and IoT,
data & analytics and the manufacturing or public sectors among others.

HPE is also a development partner of Debian,
and providing hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

### Contact Information

For further information, please visit the DebConf18 web page at
[https://debconf18.debconf.org/](https://debconf18.debconf.org/)
or send mail to <press@debian.org>.

This news item was originally posted in the [Debian blog][].

[Debian blog]: https://bits.debian.org/2018/08/debconf18-closes.html
