---
title: Call for Proposals
---

The Call for Proposals for DebConf18 has been [published](/cfp/).

You can now sign into this website and submit an event, or write to us to
suggest an invited speaker. Talk proposals must be submitted by Sunday 17 June
2018 at the latest.

We hope to see you in Hsinchu!
