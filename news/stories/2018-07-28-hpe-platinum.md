---
title: Hewlett Packard Enterprise Platinum Sponsor of DebConf18
---

We are very pleased to announce that [**Hewlett Packard Enterprise (HPE)**][hpe]
has committed support to [DebConf18][] as a **Platinum sponsor**.

*"Hewlett Packard Enterprise is excited to support Debian's annual developer
conference for the third consecutive year,"* said Steve Geary, Senior Director R&D,
Advanced Software Development, Hewlett Packard Labs.
*"The Debian community and open distribution are true innovation enablers for our
Memory-Driven Computing work and products all across HPE."*

HPE is an industry-leading technology company
providing a comprehensive portfolio of products such as
integrated systems, servers, storage, networking and software.
The company offers consulting, operational support, financial services,
and complete solutions for many different industries: mobile and IoT,
data & analytics and the manufacturing or public sectors among others.

HPE is also a development partner of Debian,
and providing hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines][] page).

With this additional commitment as Platinum Sponsor,
HPE contributes to make possible our annual conference
and directly supports the progress of Debian and Free Software,
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Hewlett Packard Enterprise, for your support of DebConf18!


This news item was originally posted in the [Debian blog][].

[hpe]: http://www.hpe.com/engage/opensource
[DebConf18]: https://debconf18.debconf.org/
[Debian machines]: https://db.debian.org/machines.cgi
[Debian blog]: https://bits.debian.org/2018/07/hpe-platinum-debconf18.html
