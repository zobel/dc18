---
title: DebConf18 starts today in Hsinchu
---

[DebConf18][], the 19th annual
Debian Conference, is taking place in Hsinchu, Taiwan 
from July 29th to August 5th, 2018.

Debian contributors from all over the world have come together at 
[National Chiao Tung University, Microelectronics and Information Research Center (NCTU MIRC)](https://wiki.debconf.org/wiki/DebConf18/Venue) 
during the preceding week for DebCamp (focused on individual work
and team sprints for in-person collaboration developing Debian), 
and the Open Day on July 28th (with presentations and workshops
of interest to a wide audience). 

Today the main conference starts with over 300 attendants 
and 118 activities scheduled, 
including 45- and 20-minute talks and team meetings, 
workshops, a job fair, talks from invited speakers, 
as well as a variety of other events.

The full schedule at
[https://debconf18.debconf.org/schedule/](/schedule/)
is updated every day, including activities planned ad-hoc 
by attendees during the whole conference.

If you want to engage remotely, you can follow the 
[**video streaming** available from the DebConf18 website](https://debconf18.debconf.org/) 
of the events happening in the three talk rooms: 
Yushan (玉山) (the main auditorium), Xueshan (雪山), and Zhongyangjianshan (中央尖山),
or join the conversation about what is happening
in the talk rooms:
  [**#debconf18-y**](https://webchat.oftc.net/?channels=#debconf18-y),
  [**#debconf18-x**](https://webchat.oftc.net/?channels=#debconf18-x) and 
  [**#debconf18-z**](https://webchat.oftc.net/?channels=#debconf18-z)
(all those channels in the OFTC IRC network). 

DebConf is committed to a safe and welcome environment for all participants. 
See the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml) 
and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct) for more details on this.

Debian thanks the commitment of numerous [sponsors](https://debconf18.debconf.org/sponsors/) 
to support DebConf18, particularly our Platinum Sponsor
[**Hewlett Packard Enterprise**][hpe].


This news item was originally posted in the [Debian blog][].

[hpe]: http://www.hpe.com/engage/opensource
[DebConf18]: https://debconf18.debconf.org/
[Debian blog]: https://bits.debian.org/2018/07/dc18-starts.html
