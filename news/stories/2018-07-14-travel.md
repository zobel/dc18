---
title: DebConf18 Arrival Information
---

If you are going to DebConf18, you'll be meeting your fellow Debian
contributors and the awesome local team soon.

Below is the link to the Welcome PDF document to facilitate your
journey and arrival at DebConf18.

Please read the information carefully, print the document and carry it
with you:

  [https://deb.li/dc18travel](https://deb.li/dc18travel)

In case of any problems (including delays, cancellation or lost in
translation), please contact <registration@debconf.org> as soon as
possible, or phone the Front Desk at +886-70-1017-2223 (we are at UTC+8,
so while traveling abroad please call with timezones in mind).

We look forward to seeing you in Hsinchu!

The DebConf team

