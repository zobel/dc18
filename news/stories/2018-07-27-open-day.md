---
title: DebConf18 invites you to Debian Open Day at National Chiao Tung University, Microelectronics and Information Research Center (NCTU MIRC), in Hsinchu
---

Translation: [DebConf18 邀你參加在新竹交通大學電子與資訊研究中心舉辦的 Debian 開放日 (Open Day)](https://bits.debian.org/2018/07/debconf18-open-day-zh-TW.html)

[DebConf](https://debconf18.debconf.org), the annual conference for Debian contributors 
and users interested in improving the [Debian operating system](https://www.debian.org), 
will be held in [National Chiao Tung University, Microelectronics and Information Research Center (NCTU MIRC)](https://wiki.debconf.org/wiki/DebConf18/Venue) 
in Hsinchu, Taiwan, from July 29th to August 5th, 2018. The conference is preceded by DebCamp, 
July 21th to July 27th, and the DebConf18 Open Day on July 28th.

Debian is an operating system consisting entirely of free and open source software, 
and is known for its adherence to the Unix and Free Software philosophies and for its extensiveness. 
Thousands of volunteers from all over the world work together to create and maintain Debian software, 
and more than 400 are expected to attend DebConf18 to meet in person and work together more closely.

The conference features presentations and workshops, and video streams are made available in real-time and archived.

The DebConf18 Open Day, Saturday, July 28, is open to the public with events of interest to a wide audience.

The [detailed schedule of the Open Day's events](https://wiki.debconf.org/wiki/DebConf18/OpenDay) include, among others:

* Questions and Answers Session with Minister Audrey Tang, 
* Debian Meets Smart City Applications with SZ Lin
* a Debian Packaging Workshop,
* panel discussion: Story of Debian contributors around the world,
* sessions in English or Chinese about different aspects of the Debian project and community, and other free software projects like LibreOffice, Clonezilla and DRBL, LXDE/LXQt desktops, EzGo...

Everyone is welcome to attend, attendance is free, 
and it is a great possibility for interested users to meet the Debian community.

The full schedule for Open Day's events and the rest of the conference is 
at [https://debconf18.debconf.org/schedule](/schedule/) 
and the video streaming will be available at the [DebConf18 website](https://debconf18.debconf.org)

DebConf is committed to a safe and welcome environment for all participants. 
See the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml) 
and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct) for more details on this.

Debian thanks the numerous [sponsors](/sponsors/) for their commitment to DebConf18, 
particularly its Platinum Sponsor [Hewlett Packard Enterprise](http://www.hpe.com/engage/opensource), 
the [Bureau of Foreign Trade, Ministry of Economic Affairs](https://www.trade.gov.tw/English/) 
via the [MEET TAIWAN program](https://www.meettaiwan.com/en_US/index.html), 
and its venue sponsors, the [National Chiao Tung University 國立交通大學](http://www.nctu.edu.tw/) 
and the [National Center for High-performance Computing 國家高速網路與計算中心](https://www.nchc.org.tw/).

For media contacts, please contact DebConf organization: 林上智 (SZ Lin), Cell: 0911-162297

This news item was originally posted in the [Debian blog](https://bits.debian.org).
